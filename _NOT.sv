module _NOT (input logic IN, 
			 output logic OUT);
	assign OUT = ~IN;
endmodule 