module DECODER (input A,B,C,D,decoType,
                output O1,O2,O3,O4,O5,O6);
	
	reg IN_DATA = {A,B,C,D}
	reg OUT_DATA = {O1,O2,O3,O4,O5,O6}
	
	always 
    if(decoType)
      begin

      case (IN_DATA)

        4'b0001: OUT_DATA = 16'b10;
        4'b0010: OUT_DATA = 16'b100;
        4'b0011: OUT_DATA = 16'b1000;
        4'b0100: OUT_DATA = 16'b10000;
        4'b0101: OUT_DATA = 16'b100000;
        4'b0110: OUT_DATA = 16'b1000000;
        default: OUT_DATA = 16'bx;

      endcase
    else
      begin
        begin

      case (IN_DATA)

        4'b0001: OUT_DATA = 16'b10;
        4'b0011: OUT_DATA = 16'b1000;
        4'b0100: OUT_DATA = 16'b10000;
        4'b0101: OUT_DATA = 16'b100000;
        default: OUT_DATA = 16'bx;

      endcase
      end
	
	end
		
endmodule 