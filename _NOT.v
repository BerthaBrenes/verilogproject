module _NOT (input IN, output OUT);

	assign OUT = ~IN;

endmodule 