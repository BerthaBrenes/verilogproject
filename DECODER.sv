module DECODER (input logic A,B,C,D,decoType,
                output logic O1,O2,O3,O4,O5,O6);
	logic IN_DATA;
	assign IN_DATA = {A,B,C,D};
	logic OUT_DATA;
	always_comb
        begin
            if(decoType)
                begin
                    case (IN_DATA)
                        4'b0001: OUT_DATA = 16'b10;
                        4'b0010: OUT_DATA = 16'b100;
                        4'b0011: OUT_DATA = 16'b1000;
                        4'b0100: OUT_DATA = 16'b10000;
                        4'b0101: OUT_DATA = 16'b100000;
                        4'b0110: OUT_DATA = 16'b1000000;
                        default: OUT_DATA = 16'bx;
                    endcase
                end
            else
                begin
                    case (IN_DATA)
                        4'b0001: OUT_DATA = 16'b10;
                        4'b0011: OUT_DATA = 16'b1000;
                        4'b0100: OUT_DATA = 16'b10000;
                        4'b0101: OUT_DATA = 16'b100000;
                        default: OUT_DATA = 16'bx;
                    endcase
                end
        end	
	assign {O1,O2,O3,O4,O5,O6} = OUT_DATA;	
endmodule 