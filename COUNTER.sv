module COUNTER (input logic A,B,C,D, ENP,ENT, LOAD, CLR, CLK, 
					 output logic Qa,Qb,Qc,Qd);
		always_ff@(posedge CLK, posedge ENP, negedge CLR, negedge LOAD) 
			begin
				if (!CLR) 
					{Qa,Qb,Qc,Qd} <=  4'b0;
				else if (!LOAD) {Qa,Qb,Qc,Qd} <=  {Qa,Qb,Qc,Qd};
				else if (ENP) {Qa,Qb,Qc,Qd} <=  {Qa,Qb,Qc,Qd} + 4'b0001;
			end
endmodule 