module _AND (input A, B, C, output D);

	assign C = A & B & C;

endmodule 