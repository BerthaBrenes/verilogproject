module _AND (input logic A, B, C, 
			 output logic D);
	assign D = A & B & C;
endmodule 