transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+/home/berthabb/Documentos/Verilog {/home/berthabb/Documentos/Verilog/MULTIPLEXOR.v}
vlog -sv -work work +incdir+/home/berthabb/Documentos/Verilog {/home/berthabb/Documentos/Verilog/COFFEE_MACHINE.sv}
vlog -sv -work work +incdir+/home/berthabb/Documentos/Verilog {/home/berthabb/Documentos/Verilog/DECODER.sv}
vlog -sv -work work +incdir+/home/berthabb/Documentos/Verilog {/home/berthabb/Documentos/Verilog/_NOT.sv}
vlog -sv -work work +incdir+/home/berthabb/Documentos/Verilog {/home/berthabb/Documentos/Verilog/_AND.sv}
vlog -sv -work work +incdir+/home/berthabb/Documentos/Verilog {/home/berthabb/Documentos/Verilog/COUNTER.sv}
vlog -sv -work work +incdir+/home/berthabb/Documentos/Verilog {/home/berthabb/Documentos/Verilog/tbcoffee_machine.sv}

