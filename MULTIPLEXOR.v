module MULTIPLEXOR (input IN_DATA, 
						  input A,B,C, D,
						  output D0,D1,D2,D3,D4,D5,D6,D7,Y,W);
	
	logic OUT_DATA;
	assign OUT_DATA = {D0,D1,D2,D3,D4,D5,D6,D7};
	logic SELECTOR;
	assign SELECTOR = {A,B,C,D};
	always_comb
	begin
	
		case (SELECTOR)
		
			3'd0: OUT_DATA = {7'd0,IN_DATA};
			3'd1: OUT_DATA = {6'd0,IN_DATA,1'd0};
			3'd2: OUT_DATA = {5'd0,IN_DATA,2'd0};
			3'd3: OUT_DATA = {4'd0,IN_DATA,3'd0};
			3'd4: OUT_DATA = {3'd0,IN_DATA,4'd0};
			3'd5: OUT_DATA = {2'd0,IN_DATA,5'd0};
			3'd6: OUT_DATA = {1'd0,IN_DATA,6'd0};
			3'd7: OUT_DATA = {IN_DATA,7'd0};
			
		endcase
	
	end
		
endmodule 