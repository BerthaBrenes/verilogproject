module tbcoffee_machine();
	logic clk, SUGAR, COFFEE_IN, GLASS, COIN_IN;
	logic PLACE, FILL, COFFE1, COFEE2, COFFEE3, MIX, OPEN, COIN_OUT;
	// instantiate device to be tested 
	COFFEE_MACHINE dut(clk, SUGAR, COFFEE_IN, GLASS, COIN_IN, 
					   PLACE, FILL, COFFE1, COFEE2, COFFEE3, MIX, OPEN, COIN_OUT);
	// initialize test
	initial begin
		clk <= 0;
	end
	always 
		begin 
			clk <= 1; 
			# 5;
			clk <= 0; 
			# 5;
		end
	always
		begin
			//ADD R0, R1, #42
			//313029282726252423222120191817161514131211109876543210
			//1 1 1 0 0 0 1 0 1 0 0 0 0 0 0 1 0 0 0 0 0 0 0000101010
			SUGAR =  1'b0;
			COFFEE_IN   =  1'b1;
			GLASS      =  1'b1;
			COIN_IN      =  1'b1;
			#10;
			//CMP R1, R2
			//11100001010100010000000000000010
			SUGAR =  1'b1;
			COFFEE_IN   =  1'b1;
			GLASS      =  1'b1;
			COIN_IN      =  1'b1;
			# 10;	
			//SUB R2, R3, #0xFF0
			//11100010010000110010111011111111
			SUGAR =  1'b1;
			COFFEE_IN   =  1'b1;
			GLASS      =  1'b0;
			COIN_IN      =  1'b0;
			//SUB R2, R3, #0xFF0
			//11100010010000110010111011111111
			SUGAR =  1'b0;
			COFFEE_IN   =  1'b1;
			GLASS      =  1'b0;
			COIN_IN      =  1'b1;
			# 10;
			//SUB R2, R3, #0xFF0
			//11100010010000110010111011111111
			SUGAR =  1'b0;
			COFFEE_IN   =  1'b0;
			GLASS      =  1'b1;
			COIN_IN      =  1'b1;
			# 10;	
			//SUB R2, R3, #0xFF0
			//11100010010000110010111011111111
			SUGAR =  1'b1;
			COFFEE_IN   =  1'b1;
			GLASS      =  1'b0;
			COIN_IN      =  1'b0;
			# 10;
		end
endmodule