module COUNTER (input A,B,C,D, ENP,ENT LOAD, CLR, CLK, output Qa,Qb,Qc,Qd);
		
		always @(posedge CLK, posedge ENP, negedge CLR, negedge LOAD) begin
			
			if (!CLR) OUT_DATA <=  4'b0;
			else if (!LOAD) OUT_DATA <=  OUT_DATA;
			else if (ENP) OUT_DATA <=  OUT_DATA + 4'b0001;
		
		end
		
endmodule 